import api from './../../Services/weaherService';

export const FORECAST_LOAD_START = 'FORECAST_LOAD_START';
export const FORECAST_LOAD_END = 'FORECAST_LOAD_END';
export const FORECAST_LOAD_ERROR = 'FORECAST_LOAD_ERROR';

export function forecastLoadStartAction(query) {
    return {
        type: FORECAST_LOAD_START,
        payload: { query }
    }
}

export function forecastLoadEndAction(forecast) {
    return {
        type: FORECAST_LOAD_END,
        payload: { forecast }
    }
}

export function forecastLoadErrorAction(error) {
    return {
        type: FORECAST_LOAD_ERROR,
        payload: { error }
    }
}

export function forecastFetchAction(query) {
    return async (dispatch) => {
        dispatch(forecastLoadStartAction(query));
        try {
            const forecast = await api.getForecast(query);
            dispatch(forecastLoadEndAction(forecast));
        } catch (e) {
            dispatch(forecastLoadErrorAction(e));
        }
    }
}