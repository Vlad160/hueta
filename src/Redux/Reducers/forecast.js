import { FORECAST_LOAD_START, FORECAST_LOAD_END, FORECAST_LOAD_ERROR } from '../Actions/forecast';

const initialState = {
    error: null,
    forecast: null,
    loading: false
}

export default function forecastReducer(state = initialState, action) {

    switch (action.type) {

        case FORECAST_LOAD_START: {
            return { ...state, error: null, forecast: null, loading: true };
        }

        case FORECAST_LOAD_END: {
            const { forecast } = action.payload;
            return { ...state, forecast, loading: false };
        }

        case FORECAST_LOAD_ERROR: {
            const { error } = action.payload;
            return { ...state, error, loading: false };
        }
        default: {
            return state;
        }
    }

}