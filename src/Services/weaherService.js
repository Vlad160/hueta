const API_BASE = 'https://api.openweathermap.org/data/2.5';
const TOKEN = '8b5d253825b2a0b505c0a5cadbee08de';

export class WeatherService {

    getCurrentWether(city) {
        return this.doFetch('weather', { q: city, units: 'metric' });
    }

    getForecast(city) {
        return this.doFetch('forecast', { q: city, units: 'metric' })
    }

    async doFetch(url, query = {}, options = {}) {
        const defaultOptions = {
            headers: {
                Accept: 'application/json'
            }
        };
        const defaultQuery = { APPID: TOKEN };
        query = { ...defaultQuery, ...query };
        options = { ...defaultOptions, ...options };
        const queryString = Object.keys(query)
            .reduce((acc, x) => {
                acc.push(`${x}=${query[x]}`);
                return acc;
            }, [])
            .join('&');
        const endpoint = [API_BASE, url]
            .join('/')
            + (queryString ? `?${queryString}` : '');
        const result = await fetch(endpoint, options);
        if (result.ok) {
            return result.json();
        } else {
            throw result.json()
        }
    }
}

export default new WeatherService();