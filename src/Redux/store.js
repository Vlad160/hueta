import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import rootReducer from './Reducers';

const logger = createLogger({
    // ...options
});

export default createStore(rootReducer, applyMiddleware(thunk, logger))