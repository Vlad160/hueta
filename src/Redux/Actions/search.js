import api from './../../Services/weaherService';

export const SEARCH_TYPE = 'SEARCH_TYPE';
export const SEARCH_CLEAR = 'SEARCH_CLEAR';
export const SEARCH_SUBMIT = 'SEARCH_SUBMIT';
export const SEARCH_LOAD_START = 'SEARCH_LOAD_START';
export const SEARCH_LOAD_END = 'SEARCH_LOAD_END';
export const SEARCH_LOAD_ERROR = 'SEARCH_LOAD_ERROR';

export function searchFetchAction(query) {
    return async (dispatch) => {
        dispatch(searchOnTypeAction(query));
        dispatch(searchLoadStartAction());
        try {
            const weather = await api.getCurrentWether(query);
            dispatch(searchLoadEndAction(weather));
        } catch (e) {
            dispatch(searchLoadErrorAction(e))
        }

    }

}

export function searchLoadStartAction() {
    return {
        type: SEARCH_LOAD_START
    }
}

export function searchLoadErrorAction(error) {
    return {
        type: SEARCH_LOAD_ERROR,
        payload: { error }
    }
}

export function searchLoadEndAction(weather) {
    return {
        type: SEARCH_LOAD_END,
        payload: { weather }
    }
}

export function searchOnTypeAction(query) {
    return {
        type: SEARCH_TYPE,
        payload: { query }
    }
}
