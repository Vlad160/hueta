import { SEARCH_TYPE, SEARCH_LOAD_START, SEARCH_LOAD_END, SEARCH_LOAD_ERROR } from '../Actions/search';

const initialState = {
    query: '',
    loading: false,
    weather: null,
    error: null
}

export default function searchReducer(state = initialState, action) {
    switch (action.type) {
        case SEARCH_TYPE: {
            const { query } = action.payload;
            return { ...state, query }
        }
        case SEARCH_LOAD_START: {
            return { ...state, weather: null, error: null, loading: true };
        }

        case SEARCH_LOAD_END: {
            const { weather } = action.payload;
            return { ...state, loading: false, weather };
        }
        case SEARCH_LOAD_ERROR: {
            const { error } = action.payload;
            return { ...state, loading: false, error };
        }
        default:
            return state
    }
}