import React, { Component } from 'react';
import Footer from './Components/footer';
import Header from './Components/header';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Main from './Scenes/Main/Containers/Main';
import store from './Redux/store'
import './App.css';
import Forecast from './Scenes/Forecast/Containers/Forecast';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Header />
          <main className="main">
            <Router className="main-body" >
              <Switch>
                <Route exact path="/" component={Main} />
                <Route path="/forecast/:city" component={Forecast} />
              </Switch>
            </Router>
          </main>
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
