import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchFetchAction } from '../../../Redux/Actions/search';
import './Search.css';
import { debounce } from 'lodash';

const SEARCH_TIMEOUT = 250;

class Search extends Component {
  render() {

    return (
      <input 
        className="search"
        type="text"
        placeholder="Search for weather..."
        onChange={e => this.handleType(e.target.value)}
      />
    );
  }
  handleType = debounce((text) => {
    const { onType } = this.props;
    onType(text);
  }, SEARCH_TIMEOUT);
}

export const mapDispatchToProps = (dispatch) => ({ onType: (text) => dispatch(searchFetchAction(text)) })

export default connect(null, mapDispatchToProps)(Search);