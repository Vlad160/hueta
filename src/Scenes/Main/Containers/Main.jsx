import React, { Component } from 'react';
import './Main.css';
import Search from '../Components/Search';
import Spinner from '../../../Components/spinner';
import { connect } from 'react-redux';
import Weather from '../Components/Weather';
import { Link } from 'react-router-dom';


class Main extends Component {

  render() {
    const { query, loading } = this.props;
    return (
      <div className="main-component">
        <Search />
        <div>
          Query: {query}
        </div>
        <div>
          {loading && <Spinner />}
        </div>
        {this.renderWeather()}
      </div>
    );
  }

  renderWeather() {
    const { loading, weather, query } = this.props;
    if (loading || !weather) {
      return null;
    }
    return (
      <div className="main-component-weather">
        {<Weather weather={weather} />}
        <Link to={`forecast/${query}`}>3 day forecast</Link>
      </div>
    )
  }
}
const mapStateToProps = state => state.search;
export default connect(mapStateToProps)(Main);