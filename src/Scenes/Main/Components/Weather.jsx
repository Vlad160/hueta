import React, { Component } from 'react';
import './Weather.css';

const ICON_BASE = 'https://openweathermap.org/img/w';

class Weather extends Component {
    render() {
        const { weather } = this.props;
        if (!weather) {
            return null;
        }
        return (
            <div className="weather-card">
                <div className="weather-temp">
                    <img alt={weather.weather[0].main} src={`${ICON_BASE}/${weather.weather[0].icon}.png`} />
                    <span className="weather-temp-i">{weather.main.temp}°</span>
                </div>
                <div className="weather-description">
                    {weather.weather[0].main}
                </div>
                <div className="weather-details">
                    <div>Pressure: {weather.main.pressure}mb</div>
                    <div>Humidity: {weather.main.humidity}%</div>
                    <div>Visibility {weather.visibility}m</div>
                    <div>Wind speed: {weather.wind.speed}m/s</div>
                </div>
            </div>
        )
    }
}

export default Weather;