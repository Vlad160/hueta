import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forecastFetchAction } from '../../../Redux/Actions/forecast';
import Spinner from '../../../Components/spinner';
import Weather from '../../Main/Components/Weather';
import './Forecast.css';


class Forecast extends Component {

    componentDidMount() {
        const { match: { params }, getForecast } = this.props;
        const { city } = params;
        getForecast(city);
    }

    render() {
        const { loading } = this.props;
        if (loading) {
            return <Spinner />
        }
        return this.renderForecastList();
    }

    renderForecastList() {
        const { forecast } = this.props;
        if (!forecast) {
            return null;
        }
        return (
            <div className="forecast-list">
                {(forecast.list || []).map((x, i) => (
                    <div key={i} className="forecast-item">
                        <b>
                            {x['dt_txt']}
                        </b>
                        <Weather weather={x} />
                    </div>
                ))}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    getForecast: (city) => dispatch(forecastFetchAction(city))
});

const mapStateToProps = (state) => state.forecast;

export default connect(mapStateToProps, mapDispatchToProps)(Forecast);