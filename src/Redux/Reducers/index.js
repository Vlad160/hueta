import { combineReducers } from 'redux';
import searchReducer from './search';
import forecastReducer from './forecast';
export default combineReducers({ search: searchReducer, forecast: forecastReducer })